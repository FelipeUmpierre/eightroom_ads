<?php

namespace Ad\SisBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\HttpFoundation\File\File;

class BannersType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('FileUpload', 'file', array(
                'label' => 'Upload',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('code', 'textarea', array(
                'label' => 'Código',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('link', 'text', array(
                'label' => 'Link',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ad\SisBundle\Entity\Banners'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ad_sisbundle_banners';
    }
}
