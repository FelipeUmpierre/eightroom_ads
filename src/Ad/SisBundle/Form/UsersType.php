<?php

namespace Ad\SisBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsersType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label' => 'Nome',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('email', 'text', array(
                'label' => 'E-mail',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('password', 'password', array(
                'label' => 'Senha',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('usersGroups', 'entity', array(
                'class' => 'Ad\SisBundle\Entity\UsersGroups',
                'label' => 'Grupo do usuário',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('active', 'checkbox', array(
                'label' => 'Ativo',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('segments', 'entity', array(
                'class' => 'Ad\SisBundle\Entity\Segments',
                'query_builder' => function( EntityRepository $segments ) {
                    return $segments->createQueryBuilder("s");
                },
                'multiple' => true,
                'mapped' => false,
                'expanded' => true,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ad\SisBundle\Entity\Users'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ad_sisbundle_users';
    }
}
