<?php

namespace Ad\SisBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ad\SisBundle\Entity\Campaigns;
use Ad\SisBundle\Form\CampaignsType;

/**
 * Campaigns controller.
 *
 * @Route("/campaigns")
 */
class CampaignsController extends Controller
{

    /**
     * Lists all Campaigns entities.
     *
     * @Route("/", name="campaigns")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdSisBundle:Campaigns')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Campaigns entity.
     *
     * @Route("/", name="campaigns_create")
     * @Method("POST")
     * @Template("AdSisBundle:Campaigns:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Campaigns();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('campaigns_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Campaigns entity.
     *
     * @param Campaigns $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Campaigns $entity)
    {
        $form = $this->createForm(new CampaignsType(), $entity, array(
            'action' => $this->generateUrl('campaigns_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Campaigns entity.
     *
     * @Route("/new", name="campaigns_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Campaigns();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Campaigns entity.
     *
     * @Route("/{id}", name="campaigns_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdSisBundle:Campaigns')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Campaigns entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Campaigns entity.
     *
     * @Route("/{id}/edit", name="campaigns_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdSisBundle:Campaigns')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Campaigns entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Campaigns entity.
    *
    * @param Campaigns $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Campaigns $entity)
    {
        $form = $this->createForm(new CampaignsType(), $entity, array(
            'action' => $this->generateUrl('campaigns_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Campaigns entity.
     *
     * @Route("/{id}", name="campaigns_update")
     * @Method("PUT")
     * @Template("AdSisBundle:Campaigns:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdSisBundle:Campaigns')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Campaigns entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('campaigns_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Campaigns entity.
     *
     * @Route("/{id}", name="campaigns_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdSisBundle:Campaigns')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Campaigns entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('campaigns'));
    }

    /**
     * Creates a form to delete a Campaigns entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('campaigns_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
