<?php

namespace Ad\SisBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class DefaultController
 * @package Ad\SisBundle\Controller
 * @Route("/")
 */

class DefaultController extends Controller
{
    /**
     * @Route("/", name="_default")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository( "AdSisBundle:Users" )->findAll();
        $segments = $em->getRepository( "AdSisBundle:Segments" )->findAll();

        return array(
            "name" => "Eduardo",
            "users" => $users,
            "segments" => $segments,
        );
    }
}