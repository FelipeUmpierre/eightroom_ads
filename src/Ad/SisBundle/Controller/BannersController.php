<?php

namespace Ad\SisBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ad\SisBundle\Entity\Banners;
use Ad\SisBundle\Form\BannersType;

/**
 * Banners controller.
 *
 * @Route("/banners")
 */
class BannersController extends Controller
{

    /**
     * Lists all Banners entities.
     *
     * @Route("/", name="banners")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdSisBundle:Banners')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Banners entity.
     *
     * @Route("/", name="banners_create")
     * @Method("POST")
     * @Template("AdSisBundle:Banners:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Banners();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('banners_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Banners entity.
     *
     * @param Banners $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Banners $entity)
    {
        $form = $this->createForm(new BannersType(), $entity, array(
            'action' => $this->generateUrl('banners_create'),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Banners entity.
     *
     * @Route("/new", name="banners_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Banners();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Banners entity.
     *
     * @Route("/{id}", name="banners_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdSisBundle:Banners')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Banners entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Banners entity.
     *
     * @Route("/{id}/edit", name="banners_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdSisBundle:Banners')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Banners entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Banners entity.
    *
    * @param Banners $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Banners $entity)
    {
        $form = $this->createForm(new BannersType(), $entity, array(
            'action' => $this->generateUrl('banners_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }
    /**
     * Edits an existing Banners entity.
     *
     * @Route("/{id}", name="banners_update")
     * @Method("PUT")
     * @Template("AdSisBundle:Banners:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdSisBundle:Banners')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Banners entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('banners'));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Banners entity.
     *
     * @Route("/{id}", name="banners_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdSisBundle:Banners')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Banners entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('banners'));
    }

    /**
     * Creates a form to delete a Banners entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('banners_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
