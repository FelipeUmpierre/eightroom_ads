<?php

namespace Ad\SisBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ad\SisBundle\Entity\Sites;
use Ad\SisBundle\Form\SitesType;

/**
 * Sites controller.
 *
 * @Route("/sites")
 */
class SitesController extends Controller
{

    /**
     * Lists all Sites entities.
     *
     * @Route("/", name="sites")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdSisBundle:Sites')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Sites entity.
     *
     * @Route("/", name="sites_create")
     * @Method("POST")
     * @Template("AdSisBundle:Sites:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Sites();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('sites', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Sites entity.
     *
     * @param Sites $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Sites $entity)
    {
        $form = $this->createForm(new SitesType(), $entity, array(
            'action' => $this->generateUrl('sites_create'),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Sites entity.
     *
     * @Route("/novo", name="sites_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Sites();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Sites entity.
     *
     * @Route("/{id}", name="sites_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdSisBundle:Sites')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sites entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Sites entity.
     *
     * @Route("/{id}/edit", name="sites_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdSisBundle:Sites')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sites entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Sites entity.
    *
    * @param Sites $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Sites $entity)
    {
        $form = $this->createForm(new SitesType(), $entity, array(
            'action' => $this->generateUrl('sites_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }
    /**
     * Edits an existing Sites entity.
     *
     * @Route("/{id}", name="sites_update")
     * @Method("PUT")
     * @Template("AdSisBundle:Sites:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdSisBundle:Sites')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sites entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('sites'));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Sites entity.
     *
     * @Route("/{id}", name="sites_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdSisBundle:Sites')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Sites entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('sites'));
    }

    /**
     * Creates a form to delete a Sites entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sites_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
