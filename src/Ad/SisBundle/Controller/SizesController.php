<?php

    namespace Ad\SisBundle\Controller;

    use Ad\SisBundle\Entity\Sizes;
    use Ad\SisBundle\Form\SizesType;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\Request;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

    /**
     * Sizes controller.
     *
     * @Route("/tamanhos")
     */
    class SizesController extends Controller
    {

        /**
         * Lists all Sizes entities.
         *
         * @Route("/", name="sizes")
         * @Method("GET")
         * @Template()
         */
        public function indexAction()
        {
            $em = $this->getDoctrine()->getManager();

            $entities = $em->getRepository( 'AdSisBundle:Sizes' )->findAll();

            return array(
                'entities' => $entities,
            );
        }

        /**
         * Creates a new Sizes entity.
         *
         * @Route("/", name="sizes_create")
         * @Method("POST")
         * @Template("AdSisBundle:Sizes:new.html.twig")
         */
        public function createAction( Request $request )
        {
            $entity = new Sizes();
            $form = $this->createCreateForm( $entity );
            $form->handleRequest( $request );

            if( $form->isValid() )
            {
                $em = $this->getDoctrine()->getManager();
                $em->persist( $entity );
                $em->flush();

                return $this->redirect( $this->generateUrl( 'sizes' ) );
            }

            return array(
                'entity' => $entity,
                'form' => $form->createView(),
            );
        }

        /**
         * Creates a form to create a Sizes entity.
         *
         * @param Sizes $entity The entity
         *
         * @return \Symfony\Component\Form\Form The form
         */
        private function createCreateForm( Sizes $entity )
        {
            $form = $this->createForm( new SizesType(), $entity, array(
                'action' => $this->generateUrl( 'sizes_create' ),
                'method' => 'POST',
            ) )
            ;

            return $form;
        }

        /**
         * Displays a form to create a new Sizes entity.
         *
         * @Route("/new", name="sizes_new")
         * @Method("GET")
         * @Template()
         */
        public function newAction()
        {
            $entity = new Sizes();
            $form = $this->createCreateForm( $entity );

            return array(
                'entity' => $entity,
                'form' => $form->createView(),
            );
        }

        /**
         * Finds and displays a Sizes entity.
         *
         * @Route("/{id}", name="sizes_show")
         * @Method("GET")
         * @Template()
         */
        public function showAction( $id )
        {
            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository( 'AdSisBundle:Sizes' )->find( $id );

            if( !$entity )
            {
                throw $this->createNotFoundException( 'Unable to find Sizes entity.' );
            }

            $deleteForm = $this->createDeleteForm( $id );

            return array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            );
        }

        /**
         * Displays a form to edit an existing Sizes entity.
         *
         * @Route("/{id}/edit", name="sizes_edit")
         * @Method("GET")
         * @Template()
         */
        public function editAction( $id )
        {
            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository( 'AdSisBundle:Sizes' )->find( $id );

            if( !$entity )
            {
                throw $this->createNotFoundException( 'Unable to find Sizes entity.' );
            }

            $editForm = $this->createEditForm( $entity );
            $deleteForm = $this->createDeleteForm( $id );

            return array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            );
        }

        /**
         * Creates a form to edit a Sizes entity.
         *
         * @param Sizes $entity The entity
         *
         * @return \Symfony\Component\Form\Form The form
         */
        private function createEditForm( Sizes $entity )
        {
            $form = $this->createForm( new SizesType(), $entity, array(
                'action' => $this->generateUrl( 'sizes_update', array( 'id' => $entity->getId() ) ),
                'method' => 'PUT',
            ) )
            ;

            return $form;
        }

        /**
         * Edits an existing Sizes entity.
         *
         * @Route("/{id}", name="sizes_update")
         * @Method("PUT")
         * @Template("AdSisBundle:Sizes:edit.html.twig")
         */
        public function updateAction( Request $request, $id )
        {
            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository( 'AdSisBundle:Sizes' )->find( $id );

            if( !$entity )
            {
                throw $this->createNotFoundException( 'Unable to find Sizes entity.' );
            }

            $deleteForm = $this->createDeleteForm( $id );
            $editForm = $this->createEditForm( $entity );
            $editForm->handleRequest( $request );

            if( $editForm->isValid() )
            {
                $em->flush();

                return $this->redirect( $this->generateUrl( 'Sizes' ) );
            }

            return array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            );
        }

        /**
         * Deletes a Sizes entity.
         *
         * @Route("/{id}", name="sizes_delete")
         * @Method("DELETE")
         */
        public function deleteAction( Request $request, $id )
        {
            $form = $this->createDeleteForm( $id );
            $form->handleRequest( $request );

            if( $form->isValid() )
            {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository( 'AdSisBundle:Sizes' )->find( $id );

                if( !$entity )
                {
                    throw $this->createNotFoundException( 'Unable to find Sizes entity.' );
                }

                $em->remove( $entity );
                $em->flush();
            }

            return $this->redirect( $this->generateUrl( 'Sizes' ) );
        }

        /**
         * Creates a form to delete a Sizes entity by id.
         *
         * @param mixed $id The entity id
         *
         * @return \Symfony\Component\Form\Form The form
         */
        private function createDeleteForm( $id )
        {
            return $this->createFormBuilder()->setAction( $this->generateUrl( 'sizes_delete', array( 'id' => $id ) ) )->setMethod( 'DELETE' )->add( 'submit', 'submit', array( 'label' => 'Delete' ) )->getForm();
        }
}
