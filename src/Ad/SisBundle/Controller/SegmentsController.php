<?php

namespace Ad\SisBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ad\SisBundle\Entity\Segments;
use Ad\SisBundle\Form\SegmentsType;

/**
 * Segments controller.
 *
 * @Route("/segmentos")
 */
class SegmentsController extends Controller
{

    /**
     * Lists all Segments entities.
     *
     * @Route("/", name="segments")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        #$entities = $em->getRepository('AdSisBundle:Segments')->findAll();
        $segments = $em->getRepository( "AdSisBundle:Sites" )->findThat();

        return array(
            'entities' => $segments,
        );
    }

    /**
     * Creates a new Segments entity.
     *
     * @Route("/", name="segments_create")
     * @Method("POST")
     * @Template("AdSisBundle:Segments:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Segments();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('segments'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Segments entity.
     *
     * @param Segments $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Segments $entity)
    {
        $form = $this->createForm(new SegmentsType(), $entity, array(
            'action' => $this->generateUrl('segments_create'),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Segments entity.
     *
     * @Route("/novo", name="segments_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Segments();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Segments entity.
     *
     * @Route("/{id}", name="segments_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdSisBundle:Segments')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Segments entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Segments entity.
     *
     * @Route("/{id}/edit", name="segments_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdSisBundle:Segments')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Segments entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Segments entity.
    *
    * @param Segments $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Segments $entity)
    {
        $form = $this->createForm(new SegmentsType(), $entity, array(
            'action' => $this->generateUrl('segments_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }
    /**
     * Edits an existing Segments entity.
     *
     * @Route("/{id}", name="segments_update")
     * @Method("PUT")
     * @Template("AdSisBundle:Segments:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdSisBundle:Segments')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Segments entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('segments'));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Segments entity.
     *
     * @Route("/{id}", name="segments_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdSisBundle:Segments')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Segments entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('segments'));
    }

    /**
     * Creates a form to delete a Segments entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('segments_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * @Route("/loadsegment", name="segments_loadsegment")
     * @Template()
     */
    public function loadSegmentAction(Request $request)
    {
        $id = $request->request->all();

        $em = $this->getDoctrine()->getManager();

        //$segments = $em->getRepository( "AdSisBundle:Segments" )->findAll();
        $segmentsBanners = $em->getRepository( 'AdSisBundle:SegmentsBannersRepository' )->findBySegmentId( $id );

        dump( $segmentsBanners );
        exit;

        return array(
            "banners" => $banners,
        );
    }
}
