<?php

namespace Ad\SisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SitesSegments
 *
 * @ORM\Table(name="sites_segments", indexes={@ORM\Index(name="fk_sites_segments_sites1_idx", columns={"sites_id"}), @ORM\Index(name="fk_sites_segments_segments1_idx", columns={"segments_id"})})
 * @ORM\Entity
 */
class SitesSegments
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Ad\SisBundle\Entity\Segments
     *
     * @ORM\ManyToOne(targetEntity="Ad\SisBundle\Entity\Segments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="segments_id", referencedColumnName="id")
     * })
     */
    private $segments;

    /**
     * @var \Ad\SisBundle\Entity\Sites
     *
     * @ORM\ManyToOne(targetEntity="Ad\SisBundle\Entity\Sites")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sites_id", referencedColumnName="id")
     * })
     */
    private $sites;



    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return SitesSegments
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set segments
     *
     * @param \Ad\SisBundle\Entity\Segments $segments
     * @return SitesSegments
     */
    public function setSegments(\Ad\SisBundle\Entity\Segments $segments = null)
    {
        $this->segments = $segments;

        return $this;
    }

    /**
     * Get segments
     *
     * @return \Ad\SisBundle\Entity\Segments 
     */
    public function getSegments()
    {
        return $this->segments;
    }

    /**
     * Set sites
     *
     * @param \Ad\SisBundle\Entity\Sites $sites
     * @return SitesSegments
     */
    public function setSites(\Ad\SisBundle\Entity\Sites $sites = null)
    {
        $this->sites = $sites;

        return $this;
    }

    /**
     * Get sites
     *
     * @return \Ad\SisBundle\Entity\Sites 
     */
    public function getSites()
    {
        return $this->sites;
    }
}
