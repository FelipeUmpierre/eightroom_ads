<?php

namespace Ad\SisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CampaignsUsers
 *
 * @ORM\Table(name="campaigns_users", indexes={@ORM\Index(name="fk_campaigns_users_campaigns1_idx", columns={"campaigns_id"}), @ORM\Index(name="fk_campaigns_users_users1_idx", columns={"users_id"})})
 * @ORM\Entity
 */
class CampaignsUsers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="percentage", type="integer", nullable=true)
     */
    private $percentage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Ad\SisBundle\Entity\Users
     *
     * @ORM\ManyToOne(targetEntity="Ad\SisBundle\Entity\Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="users_id", referencedColumnName="id")
     * })
     */
    private $users;

    /**
     * @var \Ad\SisBundle\Entity\Campaigns
     *
     * @ORM\ManyToOne(targetEntity="Ad\SisBundle\Entity\Campaigns")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campaigns_id", referencedColumnName="id")
     * })
     */
    private $campaigns;



    /**
     * Set percentage
     *
     * @param integer $percentage
     * @return CampaignsUsers
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Get percentage
     *
     * @return integer 
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return CampaignsUsers
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return CampaignsUsers
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set users
     *
     * @param \Ad\SisBundle\Entity\Users $users
     * @return CampaignsUsers
     */
    public function setUsers(\Ad\SisBundle\Entity\Users $users = null)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Get users
     *
     * @return \Ad\SisBundle\Entity\Users 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set campaigns
     *
     * @param \Ad\SisBundle\Entity\Campaigns $campaigns
     * @return CampaignsUsers
     */
    public function setCampaigns(\Ad\SisBundle\Entity\Campaigns $campaigns = null)
    {
        $this->campaigns = $campaigns;

        return $this;
    }

    /**
     * Get campaigns
     *
     * @return \Ad\SisBundle\Entity\Campaigns 
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }
    /**
     * @var \Ad\SisBundle\Entity\Account
     */
    private $account;


    /**
     * Set account
     *
     * @param \Ad\SisBundle\Entity\Account $account
     * @return CampaignsUsers
     */
    public function setAccount(\Ad\SisBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Ad\SisBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }
}
