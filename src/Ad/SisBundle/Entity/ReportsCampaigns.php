<?php

namespace Ad\SisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReportsCampaigns
 *
 * @ORM\Table(name="reports_campaigns", indexes={@ORM\Index(name="fk_reports_campaigns_campaigns1_idx", columns={"campaigns_id"})})
 * @ORM\Entity
 */
class ReportsCampaigns
{
    /**
     * @var integer
     *
     * @ORM\Column(name="click", type="bigint", nullable=true)
     */
    private $click;

    /**
     * @var integer
     *
     * @ORM\Column(name="views", type="bigint", nullable=true)
     */
    private $views;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Ad\SisBundle\Entity\Campaigns
     *
     * @ORM\ManyToOne(targetEntity="Ad\SisBundle\Entity\Campaigns")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campaigns_id", referencedColumnName="id")
     * })
     */
    private $campaigns;



    /**
     * Set click
     *
     * @param integer $click
     * @return ReportsCampaigns
     */
    public function setClick($click)
    {
        $this->click = $click;

        return $this;
    }

    /**
     * Get click
     *
     * @return integer 
     */
    public function getClick()
    {
        return $this->click;
    }

    /**
     * Set views
     *
     * @param integer $views
     * @return ReportsCampaigns
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return integer 
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ReportsCampaigns
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set campaigns
     *
     * @param \Ad\SisBundle\Entity\Campaigns $campaigns
     * @return ReportsCampaigns
     */
    public function setCampaigns(\Ad\SisBundle\Entity\Campaigns $campaigns = null)
    {
        $this->campaigns = $campaigns;

        return $this;
    }

    /**
     * Get campaigns
     *
     * @return \Ad\SisBundle\Entity\Campaigns 
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }
}
