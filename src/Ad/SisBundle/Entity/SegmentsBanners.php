<?php

    namespace Ad\SisBundle\Entity;

    use Doctrine\Common\Collections\ArrayCollection;
    use Doctrine\ORM\Mapping as ORM;

    /**
     * SegmentsBanners
     * @ORM\Table(name="segments_banners")
     * @ORM\Entity(repositoryClass="Ad\SisBundle\Entity\SegmentsBannersRepository")
     */
    class SegmentsBanners
    {
        /**
         * @var integer
         */
        private $segmentsId;

        /**
         * @var integer
         */
        private $bannersId;

        /**
         * @ORM\ManyToOne(targetEntity="Segments", cascade={"all"}, fetch="EAGER", inversedBy="segmentsBanners")
         * @ORM\JoinColumn(name="segments_id", referencedColumnName="id")
         */
        private $segment;

        /**
         * @ORM\ManyToOne(targetEntity="Banners", cascade={"all"}, fetch="EAGER", inversedBy="segmentsBanners")
         * @ORM\JoinColumn(name="banners_id", referencedColumnName="id")
         */
        private $banner;

        /**
         * @var \DateTime
         */
        private $createdAt;

        /**
         * @var integer
         * @ORM\Column(name="id", type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="IDENTITY")
         */
        private $id;

        public function __construct()
        {
            $this->segment = new ArrayCollection();
            $this->banner = new ArrayCollection();
        }

        /**
         * Set segmentsId
         *
         * @param integer $segmentsId
         * @return SegmentsBanners
         */
        public function setSegmentsId( $segmentsId )
        {
            $this->segmentsId = $segmentsId;

            return $this;
        }

        /**
         * Get segmentsId
         *
         * @return integer
         */
        public function getSegmentsId()
        {
            return $this->segmentsId;
        }

        /**
         * Set bannersId
         *
         * @param integer $bannersId
         * @return SegmentsBanners
         */
        public function setBannersId( $bannersId )
        {
            $this->bannersId = $bannersId;

            return $this;
        }

        /**
         * Get bannersId
         *
         * @return integer
         */
        public function getBannersId()
        {
            return $this->bannersId;
        }

        /**
         * Set createdAt
         *
         * @param \DateTime $createdAt
         * @return SegmentsBanners
         */
        public function setCreatedAt( $createdAt )
        {
            $this->createdAt = $createdAt;

            return $this;
        }

        /**
         * Get createdAt
         *
         * @return \DateTime
         */
        public function getCreatedAt()
        {
            return $this->createdAt;
        }

        /**
         * Get id
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * Set segment
         *
         * @param \Ad\SisBundle\Entity\Segments $segment
         * @return UsersSegments
         */
        public function setSegment( Segments $segment = null )
        {
            $this->segment = $segment;

            return $this;
        }

        /**
         * Get segment
         *
         * @return \Ad\SisBundle\Entity\Segments
         */
        public function getSegment()
        {
            return $this->segment;
        }

        /**
         * Set user
         *
         * @param \Ad\SisBundle\Entity\Banners $banners
         * @return SegmentsBanners
         */
        public function setBanner( Banners $banner = null )
        {
            $this->banner = $banner;

            return $this;
        }

        /**
         * Get user
         *
         * @return \Ad\SisBundle\Entity\Banners
         */
        public function getBanner()
        {
            return $this->banner;
        }
    }
