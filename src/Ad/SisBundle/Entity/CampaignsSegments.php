<?php

namespace Ad\SisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CampaignsSegments
 */
class CampaignsSegments
{
    /**
     * @var integer
     */
    private $segmentsId;

    /**
     * @var integer
     */
    private $campaignsId;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set segmentsId
     *
     * @param integer $segmentsId
     * @return CampaignsSegments
     */
    public function setSegmentsId($segmentsId)
    {
        $this->segmentsId = $segmentsId;

        return $this;
    }

    /**
     * Get segmentsId
     *
     * @return integer 
     */
    public function getSegmentsId()
    {
        return $this->segmentsId;
    }

    /**
     * Set campaignsId
     *
     * @param integer $campaignsId
     * @return CampaignsSegments
     */
    public function setCampaignsId($campaignsId)
    {
        $this->campaignsId = $campaignsId;

        return $this;
    }

    /**
     * Get campaignsId
     *
     * @return integer 
     */
    public function getCampaignsId()
    {
        return $this->campaignsId;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return CampaignsSegments
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \Ad\SisBundle\Entity\Campaigns
     */
    private $campaigns;

    /**
     * @var \Ad\SisBundle\Entity\Segments
     */
    private $segments;


    /**
     * Set campaigns
     *
     * @param \Ad\SisBundle\Entity\Campaigns $campaigns
     * @return CampaignsSegments
     */
    public function setCampaigns(\Ad\SisBundle\Entity\Campaigns $campaigns = null)
    {
        $this->campaigns = $campaigns;

        return $this;
    }

    /**
     * Get campaigns
     *
     * @return \Ad\SisBundle\Entity\Campaigns 
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }

    /**
     * Set segments
     *
     * @param \Ad\SisBundle\Entity\Segments $segments
     * @return CampaignsSegments
     */
    public function setSegments(\Ad\SisBundle\Entity\Segments $segments = null)
    {
        $this->segments = $segments;

        return $this;
    }

    /**
     * Get segments
     *
     * @return \Ad\SisBundle\Entity\Segments 
     */
    public function getSegments()
    {
        return $this->segments;
    }
}
