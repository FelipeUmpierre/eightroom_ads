<?php

namespace Ad\SisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CampaignsSites
 *
 * @ORM\Table(name="campaigns_sites", indexes={@ORM\Index(name="fk_campaigns_sites_campaigns1_idx", columns={"campaigns_id"}), @ORM\Index(name="fk_campaigns_sites_sites1_idx", columns={"sites_id"})})
 * @ORM\Entity
 */
class CampaignsSites
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Ad\SisBundle\Entity\Sites
     *
     * @ORM\ManyToOne(targetEntity="Ad\SisBundle\Entity\Sites")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sites_id", referencedColumnName="id")
     * })
     */
    private $sites;

    /**
     * @var \Ad\SisBundle\Entity\Campaigns
     *
     * @ORM\ManyToOne(targetEntity="Ad\SisBundle\Entity\Campaigns")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campaigns_id", referencedColumnName="id")
     * })
     */
    private $campaigns;



    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return CampaignsSites
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sites
     *
     * @param \Ad\SisBundle\Entity\Sites $sites
     * @return CampaignsSites
     */
    public function setSites(\Ad\SisBundle\Entity\Sites $sites = null)
    {
        $this->sites = $sites;

        return $this;
    }

    /**
     * Get sites
     *
     * @return \Ad\SisBundle\Entity\Sites 
     */
    public function getSites()
    {
        return $this->sites;
    }

    /**
     * Set campaigns
     *
     * @param \Ad\SisBundle\Entity\Campaigns $campaigns
     * @return CampaignsSites
     */
    public function setCampaigns(\Ad\SisBundle\Entity\Campaigns $campaigns = null)
    {
        $this->campaigns = $campaigns;

        return $this;
    }

    /**
     * Get campaigns
     *
     * @return \Ad\SisBundle\Entity\Campaigns 
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }
}
