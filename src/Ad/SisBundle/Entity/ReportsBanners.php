<?php

namespace Ad\SisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReportsBanners
 *
 * @ORM\Table(name="reports_banners", indexes={@ORM\Index(name="fk_reports_banners_banners1_idx", columns={"banners_id"})})
 * @ORM\Entity
 */
class ReportsBanners
{
    /**
     * @var integer
     *
     * @ORM\Column(name="click", type="bigint", nullable=true)
     */
    private $click;

    /**
     * @var integer
     *
     * @ORM\Column(name="views", type="bigint", nullable=true)
     */
    private $views;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Ad\SisBundle\Entity\Banners
     *
     * @ORM\ManyToOne(targetEntity="Ad\SisBundle\Entity\Banners")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="banners_id", referencedColumnName="id")
     * })
     */
    private $banners;



    /**
     * Set click
     *
     * @param integer $click
     * @return ReportsBanners
     */
    public function setClick($click)
    {
        $this->click = $click;

        return $this;
    }

    /**
     * Get click
     *
     * @return integer 
     */
    public function getClick()
    {
        return $this->click;
    }

    /**
     * Set views
     *
     * @param integer $views
     * @return ReportsBanners
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return integer 
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ReportsBanners
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set banners
     *
     * @param \Ad\SisBundle\Entity\Banners $banners
     * @return ReportsBanners
     */
    public function setBanners(\Ad\SisBundle\Entity\Banners $banners = null)
    {
        $this->banners = $banners;

        return $this;
    }

    /**
     * Get banners
     *
     * @return \Ad\SisBundle\Entity\Banners 
     */
    public function getBanners()
    {
        return $this->banners;
    }
}
