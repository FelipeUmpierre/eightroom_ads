<?php

namespace Ad\SisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UsersGroupsPermissions
 *
 * @ORM\Table(name="users_groups_permissions", indexes={@ORM\Index(name="fk_users_groups_permissions_users_groups1_idx", columns={"users_groups_id"})})
 * @ORM\Entity
 */
class UsersGroupsPermissions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Ad\SisBundle\Entity\UsersGroups
     *
     * @ORM\ManyToOne(targetEntity="Ad\SisBundle\Entity\UsersGroups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="users_groups_id", referencedColumnName="id")
     * })
     */
    private $usersGroups;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usersGroups
     *
     * @param \Ad\SisBundle\Entity\UsersGroups $usersGroups
     * @return UsersGroupsPermissions
     */
    public function setUsersGroups(\Ad\SisBundle\Entity\UsersGroups $usersGroups = null)
    {
        $this->usersGroups = $usersGroups;

        return $this;
    }

    /**
     * Get usersGroups
     *
     * @return \Ad\SisBundle\Entity\UsersGroups 
     */
    public function getUsersGroups()
    {
        return $this->usersGroups;
    }
}
