<?php

namespace Ad\SisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccountCashierHistory
 */
class AccountCashierHistory
{
    /**
     * @var string
     */
    private $balanceOld;

    /**
     * @var string
     */
    private $balanceNew;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Ad\SisBundle\Entity\AccountCashier
     */
    private $accountCashier;


    /**
     * Set balanceOld
     *
     * @param string $balanceOld
     * @return AccountCashierHistory
     */
    public function setBalanceOld($balanceOld)
    {
        $this->balanceOld = $balanceOld;

        return $this;
    }

    /**
     * Get balanceOld
     *
     * @return string 
     */
    public function getBalanceOld()
    {
        return $this->balanceOld;
    }

    /**
     * Set balanceNew
     *
     * @param string $balanceNew
     * @return AccountCashierHistory
     */
    public function setBalanceNew($balanceNew)
    {
        $this->balanceNew = $balanceNew;

        return $this;
    }

    /**
     * Get balanceNew
     *
     * @return string 
     */
    public function getBalanceNew()
    {
        return $this->balanceNew;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return AccountCashierHistory
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accountCashier
     *
     * @param \Ad\SisBundle\Entity\AccountCashier $accountCashier
     * @return AccountCashierHistory
     */
    public function setAccountCashier(\Ad\SisBundle\Entity\AccountCashier $accountCashier = null)
    {
        $this->accountCashier = $accountCashier;

        return $this;
    }

    /**
     * Get accountCashier
     *
     * @return \Ad\SisBundle\Entity\AccountCashier 
     */
    public function getAccountCashier()
    {
        return $this->accountCashier;
    }
}
