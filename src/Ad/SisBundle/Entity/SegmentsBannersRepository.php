<?php

namespace Ad\SisBundle\Entity;

use Doctrine\ORM\EntityRepository;

class SegmentsBannersRepository extends EntityRepository
{
    public function findBySegmentId( $id )
    {
        $repository = $this->_em->getRepository( "AdSisBundle:SegmentsBanners" );

        $query = $repository->createQueryBuilder( "sb" )
                            ->select( array( "s", "b", "sb" ) )
                            ->join( "sb.segment", "s" )
                            ->join( "sb.banner", "b" )
                            ->where( "s.id = :id" )
                            ->setParameter( "id", $id )
        ;

        return $query->getResult();
    }
}