<?php

namespace Ad\SisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReportsSites
 *
 * @ORM\Table(name="reports_sites", indexes={@ORM\Index(name="fk_reports_sites_sites1_idx", columns={"sites_id"}), @ORM\Index(name="fk_reports_sites_banners1_idx", columns={"banners_id"})})
 * @ORM\Entity
 */
class ReportsSites
{
    /**
     * @var integer
     *
     * @ORM\Column(name="click", type="bigint", nullable=true)
     */
    private $click;

    /**
     * @var integer
     *
     * @ORM\Column(name="views", type="bigint", nullable=true)
     */
    private $views;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Ad\SisBundle\Entity\Banners
     *
     * @ORM\ManyToOne(targetEntity="Ad\SisBundle\Entity\Banners")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="banners_id", referencedColumnName="id")
     * })
     */
    private $banners;

    /**
     * @var \Ad\SisBundle\Entity\Sites
     *
     * @ORM\ManyToOne(targetEntity="Ad\SisBundle\Entity\Sites")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sites_id", referencedColumnName="id")
     * })
     */
    private $sites;



    /**
     * Set click
     *
     * @param integer $click
     * @return ReportsSites
     */
    public function setClick($click)
    {
        $this->click = $click;

        return $this;
    }

    /**
     * Get click
     *
     * @return integer 
     */
    public function getClick()
    {
        return $this->click;
    }

    /**
     * Set views
     *
     * @param integer $views
     * @return ReportsSites
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return integer 
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ReportsSites
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set banners
     *
     * @param \Ad\SisBundle\Entity\Banners $banners
     * @return ReportsSites
     */
    public function setBanners(\Ad\SisBundle\Entity\Banners $banners = null)
    {
        $this->banners = $banners;

        return $this;
    }

    /**
     * Get banners
     *
     * @return \Ad\SisBundle\Entity\Banners 
     */
    public function getBanners()
    {
        return $this->banners;
    }

    /**
     * Set sites
     *
     * @param \Ad\SisBundle\Entity\Sites $sites
     * @return ReportsSites
     */
    public function setSites(\Ad\SisBundle\Entity\Sites $sites = null)
    {
        $this->sites = $sites;

        return $this;
    }

    /**
     * Get sites
     *
     * @return \Ad\SisBundle\Entity\Sites 
     */
    public function getSites()
    {
        return $this->sites;
    }
}
