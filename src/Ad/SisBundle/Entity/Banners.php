<?php

namespace Ad\SisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Banners
 *
 * @ORM\Table(name="banners", indexes={@ORM\Index(name="fk_banners_file_upload1_idx", columns={"file_upload_id"})})
 * @ORM\Entity
 */
class Banners
{
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="text", nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="text", nullable=true)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=65, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Ad\SisBundle\Entity\FileUpload
     *
     * @ORM\ManyToOne(targetEntity="FileUpload", cascade={"all"}, fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="file_upload_id", referencedColumnName="id")
     * })
     */
    private $fileUpload;

    /**
     * Set code
     *
     * @param string $code
     * @return Banners
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return Link
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Banners
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fileUpload
     *
     * @param \Ad\SisBundle\Entity\FileUpload $fileUpload
     * @return Banners
     */
    public function setFileUpload(\Ad\SisBundle\Entity\FileUpload $fileUpload = null)
    {
        $this->fileUpload = $fileUpload;

        return $this;
    }

    /**
     * Get fileUpload
     *
     * @return \Ad\SisBundle\Entity\FileUpload
     */
    public function getFileUpload()
    {
        return $this->fileUpload;
    }

    /**
     * @var \Ad\SisBundle\Entity\Sizes
     */
    private $sizes;


    /**
     * Set sizes
     *
     * @param \Ad\SisBundle\Entity\Sizes $sizes
     * @return Banners
     */
    public function setSizes(\Ad\SisBundle\Entity\Sizes $sizes = null)
    {
        $this->sizes = $sizes;

        return $this;
    }

    /**
     * Get sizes
     *
     * @return \Ad\SisBundle\Entity\Sizes
     */
    public function getSizes()
    {
        return $this->sizes;
    }
}