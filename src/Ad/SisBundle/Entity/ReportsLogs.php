<?php

namespace Ad\SisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReportsLogs
 *
 * @ORM\Table(name="reports_logs", indexes={@ORM\Index(name="fk_reports_logs_campaigns1_idx", columns={"campaigns_id"}), @ORM\Index(name="fk_reports_logs_banners1_idx", columns={"banners_id"}), @ORM\Index(name="fk_reports_logs_users1_idx", columns={"users_id"})})
 * @ORM\Entity
 */
class ReportsLogs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="action", type="integer", nullable=true)
     */
    private $action;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Ad\SisBundle\Entity\Users
     *
     * @ORM\ManyToOne(targetEntity="Ad\SisBundle\Entity\Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="users_id", referencedColumnName="id")
     * })
     */
    private $users;

    /**
     * @var \Ad\SisBundle\Entity\Banners
     *
     * @ORM\ManyToOne(targetEntity="Ad\SisBundle\Entity\Banners")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="banners_id", referencedColumnName="id")
     * })
     */
    private $banners;

    /**
     * @var \Ad\SisBundle\Entity\Campaigns
     *
     * @ORM\ManyToOne(targetEntity="Ad\SisBundle\Entity\Campaigns")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campaigns_id", referencedColumnName="id")
     * })
     */
    private $campaigns;



    /**
     * Set action
     *
     * @param integer $action
     * @return ReportsLogs
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return integer 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ReportsLogs
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set users
     *
     * @param \Ad\SisBundle\Entity\Users $users
     * @return ReportsLogs
     */
    public function setUsers(\Ad\SisBundle\Entity\Users $users = null)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Get users
     *
     * @return \Ad\SisBundle\Entity\Users 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set banners
     *
     * @param \Ad\SisBundle\Entity\Banners $banners
     * @return ReportsLogs
     */
    public function setBanners(\Ad\SisBundle\Entity\Banners $banners = null)
    {
        $this->banners = $banners;

        return $this;
    }

    /**
     * Get banners
     *
     * @return \Ad\SisBundle\Entity\Banners 
     */
    public function getBanners()
    {
        return $this->banners;
    }

    /**
     * Set campaigns
     *
     * @param \Ad\SisBundle\Entity\Campaigns $campaigns
     * @return ReportsLogs
     */
    public function setCampaigns(\Ad\SisBundle\Entity\Campaigns $campaigns = null)
    {
        $this->campaigns = $campaigns;

        return $this;
    }

    /**
     * Get campaigns
     *
     * @return \Ad\SisBundle\Entity\Campaigns 
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }
}
