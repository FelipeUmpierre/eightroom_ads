<?php

namespace Ad\SisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BannersConfigurations
 */
class BannersConfigurations
{
    /**
     * @var integer
     */
    private $bannersId;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set bannersId
     *
     * @param integer $bannersId
     * @return BannersConfigurations
     */
    public function setBannersId($bannersId)
    {
        $this->bannersId = $bannersId;

        return $this;
    }

    /**
     * Get bannersId
     *
     * @return integer 
     */
    public function getBannersId()
    {
        return $this->bannersId;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \Ad\SisBundle\Entity\Banners
     */
    private $banners;


    /**
     * Set banners
     *
     * @param \Ad\SisBundle\Entity\Banners $banners
     * @return BannersConfigurations
     */
    public function setBanners(\Ad\SisBundle\Entity\Banners $banners = null)
    {
        $this->banners = $banners;

        return $this;
    }

    /**
     * Get banners
     *
     * @return \Ad\SisBundle\Entity\Banners 
     */
    public function getBanners()
    {
        return $this->banners;
    }
}
