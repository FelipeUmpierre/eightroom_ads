<?php

namespace Ad\SisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CampaignsBanners
 *
 * @ORM\Table(name="campaigns_banners", indexes={@ORM\Index(name="fk_campaigns_banners_banners1_idx", columns={"banners_id"}), @ORM\Index(name="fk_campaigns_banners_campaigns1_idx", columns={"campaigns_id"})})
 * @ORM\Entity
 */
class CampaignsBanners
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Ad\SisBundle\Entity\Campaigns
     *
     * @ORM\ManyToOne(targetEntity="Ad\SisBundle\Entity\Campaigns")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campaigns_id", referencedColumnName="id")
     * })
     */
    private $campaigns;

    /**
     * @var \Ad\SisBundle\Entity\Banners
     *
     * @ORM\ManyToOne(targetEntity="Ad\SisBundle\Entity\Banners")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="banners_id", referencedColumnName="id")
     * })
     */
    private $banners;



    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return CampaignsBanners
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set campaigns
     *
     * @param \Ad\SisBundle\Entity\Campaigns $campaigns
     * @return CampaignsBanners
     */
    public function setCampaigns(\Ad\SisBundle\Entity\Campaigns $campaigns = null)
    {
        $this->campaigns = $campaigns;

        return $this;
    }

    /**
     * Get campaigns
     *
     * @return \Ad\SisBundle\Entity\Campaigns 
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }

    /**
     * Set banners
     *
     * @param \Ad\SisBundle\Entity\Banners $banners
     * @return CampaignsBanners
     */
    public function setBanners(\Ad\SisBundle\Entity\Banners $banners = null)
    {
        $this->banners = $banners;

        return $this;
    }

    /**
     * Get banners
     *
     * @return \Ad\SisBundle\Entity\Banners 
     */
    public function getBanners()
    {
        return $this->banners;
    }
}
