<?php

namespace Ad\SisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccountCashier
 */
class AccountCashier
{
    /**
     * @var string
     */
    private $balance;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Ad\SisBundle\Entity\Account
     */
    private $account;


    /**
     * Set balance
     *
     * @param string $balance
     * @return AccountCashier
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return string 
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return AccountCashier
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set account
     *
     * @param \Ad\SisBundle\Entity\Account $account
     * @return AccountCashier
     */
    public function setAccount(\Ad\SisBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Ad\SisBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }
}
