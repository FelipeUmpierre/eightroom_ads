<?php

namespace Ad\SisBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * UsersSegments
 *
 * @ORM\Table(name="users_segments", indexes={@ORM\Index(name="fk_user_segment_user_idx", columns={"user_id"}), @ORM\Index(name="fk_user_segment_segment1_idx", columns={"segment_id"})})
 * @ORM\Entity(repositoryClass="Ad\SisBundle\Entity\UsersSegmentsRepository")
 */
class UsersSegments
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Ad\SisBundle\Entity\Segments", inversedBy="usersSegments")
     * @ORM\JoinColumn(name="segment_id", referencedColumnName="id")
     */
    private $segment;

    /**
     * @ORM\ManyToOne(targetEntity="Ad\SisBundle\Entity\Users", inversedBy="usersSegments")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    public function __construct()
    {
        $this->user = new ArrayCollection();
        $this->segment = new ArrayCollection();
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return UsersSegments
     */
    public function setCreatedAt( $createdAt )
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set segment
     *
     * @param \Ad\SisBundle\Entity\Segments $segment
     * @return UsersSegments
     */
    public function setSegment( \Ad\SisBundle\Entity\Segments $segment = null )
    {
        $this->segment = $segment;

        return $this;
    }

    /**
     * Get segment
     *
     * @return \Ad\SisBundle\Entity\Segments
     */
    public function getSegment()
    {
        return $this->segment;
    }

    /**
     * Set user
     *
     * @param \Ad\SisBundle\Entity\Users $user
     * @return UsersSegments
     */
    public function setUser( \Ad\SisBundle\Entity\Users $user = null )
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Ad\SisBundle\Entity\Users
     */
    public function getUser()
    {
        return $this->user;
    }
}
