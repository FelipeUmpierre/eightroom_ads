jQuery( document ).ready( function() {
    Metronic.init();

    $('.fancybox').fancybox({
        padding: 0,
        openEffect: 'fade',
        closeEffect: 'fade',
        closeClick: true
    });

    checkExternal();
} );

$.fn.OneClickSelect = function () {
    return $(this).on('mouseenter', function () {

        // In here, "this" is the element

        var range, selection;

        // non-IE browsers account for 60% of users, this means 60% of the time,
        // the two conditions are evaluated since you check for IE first.

        // Instead, reverse the conditions to get it right on the first check 60% of the time.

        if (window.getSelection) {
            selection = window.getSelection();
            range = document.createRange();
            range.selectNodeContents(this);
            selection.removeAllRanges();
            selection.addRange(range);
        } else if (document.body.createTextRange) {
            range = document.body.createTextRange();
            range.moveToElementText(this);
            range.select();
        }
    });
};

// Apply to these elements
$('.codeselect').OneClickSelect();

function checkExternal() {
    if( $( ".externalCheck" ).is( ":checked" ) ) {
        $( ".externalLink").hide();
        $( ".externalCode").fadeIn( 300 );
    } else {
        $( ".externalCode").hide();
        $( ".externalLink").fadeIn( 300 );
    }
}

$( document ).on( "change", ".externalCheck", function() {
    checkExternal();
} )

$( document ).on( "click", ".btn-loadaction", function( e ) {
    e.preventDefault();

    // Pega as informações
    var id = $( this ).data( "id" );
    var path = $( this ).data( "path" );
    var step = $( this ).data( "step" );
    var _class = "." + step;

    $( _class ).find( ".inner-content" ).remove();
    $( _class ).append( "<div class='inner-content'></div>" );

    // Coloca status de ativo no link
    $( this ).parent().parent().find( "a" ).removeClass( "active" );
    $( this ).addClass( "active" );

    // Esconde o texto e mostra o loader
    $( _class + " .text" ).hide();
    $( _class + " .loader" ).fadeIn( 300 );

    // Faz o post
    $.post(
        path,
        { id : id },
        function( e )
        {
            // Ao finalizar, esconde o loader
            $( _class + " .loader" ).hide();

            // Se voltar vazio (0), mostra o texto com mensagem específica, senão, mostra o conteúdo
            if( e == 0 )
            {
                $( _class + " .text" ).text( "Nenhum registro encontrado" );
                $( _class + " .text" ).fadeIn( 300 );
            }
            else {
                $( _class ).find( ".inner-content" ).html( e );
            }
        }
    )
    .fail( function()
    {
        // Ao finalizar, esconde o loader
        $( _class + " .loader" ).hide();

        // Se der erro, mostra o texto com mensagem específica
        $( _class + " .text" ).text( "Um erro aconteceu, tente novamente" );
        $( _class + " .text" ).fadeIn( 300 );
    } )
} );